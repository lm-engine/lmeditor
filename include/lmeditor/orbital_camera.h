#pragma once

#include <lmlib/camera.h>

namespace lmeditor
{
struct orbital_camera_init;
class orbital_camera : public lm::camera
{
  public:
    orbital_camera() = default;
    orbital_camera(const orbital_camera_init &init);

    orbital_camera &rotate(const Eigen::Vector3f &delta_angles);
    orbital_camera &move(const float distance);
    orbital_camera &move_to_target(const Eigen::Vector3f &target);

    Eigen::Vector3f get_pos(
      const Eigen::Vector3f &angles,
      const Eigen::Vector3f &target,
      float distance);

    float distance;
};

struct orbital_camera_init
{
    float fov, aspect, near_clip, far_clip, distance;
    Eigen::Vector3f target, euler_angles;
};
} // namespace lmeditor
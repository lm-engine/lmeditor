#include "command_help.h"

#include <lmtk/text_layout.h>
#include <lmtk/vlayout.h>

namespace lmeditor
{
std::vector<std::array<lmtk::text_layout, 3>>
  create_command_rows(command_help_init const &init)
{
    std::vector<std::array<lmtk::text_layout, 3>> rows;
    lmtk::text_layout_factory layout_factory{
      init.renderer, init.material, init.font_atlas, .colour = {1.f, 1.f, 1.f}};
    rows.emplace_back(std::array{layout_factory.create("Command"),
                                 layout_factory.create("Keystroke"),
                                 layout_factory.create("Context")});
    for (auto const &command : init.commands)
    {
        rows.emplace_back(std::array{layout_factory.create(command.name),
                                     layout_factory.create(command.key),
                                     layout_factory.create(command.context)});
    }
    return std::move(rows);
}

command_help::command_help(command_help_init const &init)
    : rows{create_command_rows(init)}, table{rows}
{
}

lmtk::iwidget &command_help::add_to_frame(lmgl::iframe *frame)
{
    for (auto &row : rows)
        for (auto &text_layout : row)
            text_layout.render(frame);
    return *this;
}

lm::size2i command_help::get_size()
{
    throw std::runtime_error{"Not implemented."};
}

lm::point2i command_help::get_position()
{
    throw std::runtime_error{"Not implemented."};
}

lmtk::iwidget &command_help::set_rect(lm::point2i position, lm::size2i size)
{
    throw std::runtime_error{"Not implemented."};
}

lmtk::iwidget &command_help::move_resources(
  lmgl::irenderer *renderer,
  lmtk::resource_sink &resource_sink)
{
    for (auto &row : rows)
        for (auto &layout : row)
            layout.move_resources(renderer, resource_sink);

    return *this;
}
} // namespace lmeditor

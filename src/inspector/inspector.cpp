#include "inspector.h"

#include <entt/meta/meta.hpp>
#include <fmt/format.h>

#include <lmengine/reflection.h>
#include <lmlib/enumerate.h>
#include <lmlib/variant_visitor.h>
#include <lmtk/text_editor.h>
#include <lmtk/text_layout.h>
#include <lmtk/vlayout.h>
#include <range/v3/algorithm/remove.hpp>

namespace lmeditor
{
pinspector create_inspector(
  lmgl::irenderer &renderer,
  lmgl::material text_material,
  lmtk::font_atlas &font_atlas,
  lm::size2i const &size)
{
    return std::make_unique<inspector>(
      renderer, text_material, font_atlas, size);
}

inspector::inspector(
  lmgl::irenderer &renderer,
  lmgl::material text_material,
  lmtk::font_atlas &font_atlas,
  lm::size2i const &size)
    : renderer{renderer},
      size{size},
      text_material{text_material},
      font_atlas{font_atlas},
      background_material{lmtk::rect::create_material(&renderer)},
      background{
        renderer,
        background_material,
        {0, 0},
        size,
        std::array{0.f, 0.f, 0.f, 1.f},
      },
      state{empty_state{}},
      entity{entt::null}
{
}

void inspector::display(
  entt::registry &registry,
  entt::entity entity,
  lmtk::resource_sink &resource_sink)
{
    clear(resource_sink);
    this->entity = entity;

    create_text(registry);
    state = select_state{};
}

void inspector::create_text(entt::registry &registry)
{
    bool first_line{true};
    lmng::reflect_components(
      registry, entity, [&](lmng::any_component const &component) {
          auto init = lmtk::text_layout_init{
            .renderer = renderer,
            .material = text_material,
            .atlas = font_atlas,
            .colour = {1.f, 1.f, 1.f},
            .text = component.name(),
          };
          text_lines.emplace_back(lmtk::text_layout{init});

          component.any.type().data([&](entt::meta_data data) {
              auto text_repr = component.get(data);
              auto title = lmng::get_data_name(data);

              auto line_text = format_component_data(title, text_repr);
              if (first_line)
              {
                  line_text[1] = '*';
                  first_line = false;
              }

              auto text_layout_init = lmtk::text_layout_init{
                .renderer = renderer,
                .material = text_material,
                .atlas = font_atlas,
                .colour = {1.f, 1.f, 1.f},
                .text = line_text,
              };
              data_entries.emplace_back(data_entry{
                text_repr,
                data,
                static_cast<unsigned int>(text_lines.size()),
              });
              text_lines.emplace_back(lmtk::text_layout{text_layout_init});
          });
      });
    lmtk::layout_vertical(
      lmtk::vertical_layout{.position = {5, 0}, .vertical_spacing = 15},
      text_lines);
}

void inspector::update(
  entt::registry &registry,
  lmtk::resource_sink &resource_sink)
{
    clear_text(resource_sink);
    create_text(registry);
}

void inspector::clear(lmtk::resource_sink &resource_sink)
{
    clear_text(resource_sink);

    selected_data_index = 0;
    state = empty_state{};
    entity = entt::null;
}

void inspector::clear_text(lmtk::resource_sink &resource_sink)
{
    for (auto &line : text_lines)
        line.move_resources(nullptr, resource_sink);

    text_lines.clear();
    data_entries.clear();
}

bool inspector::handle(
  lmtk::input_event const &event,
  lmtk::resource_sink &resource_sink,
  inspector_event_handler const &event_handler)
{
    return state >> lm::variant_visitor{[&](auto &state_alternative) {
               return handle(
                 state_alternative, {event, resource_sink, event_handler});
           }};
}

inspector &inspector::add_to_frame(lmgl::iframe *frame)
{
    render_base(frame);

    state >> lm::variant_visitor{
               [&](add_state &add_state) { render(add_state, frame); },
               [](auto) {},
             };
    return *this;
}

void inspector::render_base(lmgl::iframe *frame)
{
    background.add_to_frame(frame);
    for (auto &line : text_lines)
    {
        line.render(frame);
    }
}

bool inspector::move_selection(int movement, lmtk::resource_sink &resource_sink)
{
    int new_pos = std::max(
      std::min(selected_data_index + movement, (int)data_entries.size() - 1),
      0);
    if (selected_data_index == new_pos)
        return false;

    update_data_line(
      data_entries[selected_data_index], false, resource_sink, selected_line());

    selected_data_index = new_pos;
    update_data_line(
      data_entries[selected_data_index], true, resource_sink, selected_line());

    return true;
}

void inspector::update_data_line(
  data_entry &entry,
  bool selected,
  lmtk::resource_sink &resource_sink,
  lmtk::text_layout &line_layout)
{
    auto new_text =
      format_component_data(lmng::get_data_name(entry.data), entry.data_str);
    if (selected)
        new_text[1] = '*';

    line_layout.set_text(renderer, font_atlas, new_text, resource_sink);
}

std::string inspector::format_component_data(
  std::string const &name,
  std::string const &repr)
{
    return fmt::format("  {}: {}", name, repr);
}

bool inspector::handle(inspector::empty_state &, state_handle_args const &args)
{
    return false;
}

bool inspector::handle(select_state &, state_handle_args const &args)
{
    return args.input_event >>
           lm::variant_visitor{
             [&](lmtk::key_down_event const &key_down_event) {
                 switch (key_down_event.key)
                 {
                 case lmpl::key_code::I:
                 {
                     return move_selection(-1, args.resource_sink);
                 }

                 case lmpl::key_code::K:
                 {
                     return move_selection(1, args.resource_sink);
                 }

                 case lmpl::key_code::Enter:
                 {
                     auto &entry = selection();
                     state = edit_state{lmtk::text_editor{entry.data_str}};
                     return true;
                 }

                 case lmpl::key_code::A:
                 {
                     state = create_add_state();
                     return true;
                 }

                 default:
                     return false;
                 }
             },
             [&](auto) { return false; },
           };
}

bool inspector::handle(edit_state &edit_state, state_handle_args const &args)
{
    return args.input_event >>
           lm::variant_visitor{
             [&](lmtk::key_down_event const &key_down_msg) {
                 if (edit_state.editor.handle(key_down_msg))
                 {
                     auto &entry = selection();
                     auto &line = selected_line();
                     line.set_text(
                       renderer,
                       font_atlas,
                       std::string{" *"} + fmt::format(
                                             "{}: {}",
                                             lmng::get_data_name(entry.data),
                                             edit_state.editor.text),
                       args.resource_sink);
                     return true;
                 }
                 switch (key_down_msg.key)
                 {
                 case lmpl::key_code::Enter:
                     args.event_handler(inspector_updated_data{
                       selected_data(), edit_state.editor.text});
                     selection().data_str = edit_state.editor.text;
                     state = select_state{};
                     return true;

                 default:
                     return false;
                 }
             },
             [](auto) { return false; },
           };
}

inspector::add_state inspector::create_add_state()
{
    lm::point2i pos{size.width, 0};
    lmtk::rect background{
      renderer,
      background_material,
      pos,
      size,
      {0.f, 0.f, 0.f, 1.f},
    };
    lmtk::text_layout title_line{
      lmtk::text_layout_init{
        renderer,
        text_material,
        font_atlas,
        {1.f, 1.f, 1.f},
        pos,
        "Add Component",
      },
    };

    std::vector<add_state::entry> entries;
    int height{32};
    int index{0};
    entt::resolve([&](entt::meta_type const &component_type) {
        auto entry_text =
          fmt::format("  {}", lmng::get_type_name(component_type));
        if (index++ == 0)
            entry_text[1] = '*';

        lmtk::text_layout line{
          lmtk::text_layout_init{
            renderer,
            text_material,
            font_atlas,
            {1.f, 1.f, 1.f},
            {pos.x, pos.y + height},
            entry_text,
          },
        };
        entries.emplace_back(add_state::entry{component_type, std::move(line)});
        height += 32;
    });

    auto state = add_state{
      .background = std::move(background),
      .title_line = std::move(title_line),
      .entries = std::move(entries),
    };
    return std::move(state);
}

void inspector::render(add_state &add_state, lmgl::iframe *frame)
{
    add_state.background.add_to_frame(frame);
    add_state.title_line.render(frame);
    for (auto &data : add_state.entries)
    {
        data.line.render(frame);
    }
}

bool inspector::handle(
  inspector::add_state &add_state,
  state_handle_args const &args)
{
    return args.input_event >>
           lm::variant_visitor{
             [&](lmtk::key_down_event const &key_down_msg) {
                 switch (key_down_msg.key)
                 {
                 case lmpl::key_code::I:
                     return move_selection(add_state, -1, args.resource_sink);

                 case lmpl::key_code::K:
                     return move_selection(add_state, 1, args.resource_sink);

                 case lmpl::key_code::Enter:
                     args.event_handler(
                       inspector_added_component{add_state.selection().type});
                     destroy(add_state, args.resource_sink);
                     state = select_state{};
                     return true;

                 default:
                     return false;
                 }
             },
             [](auto) { return false; },
           };
}

bool inspector::move_selection(
  add_state &add_state,
  int movement,
  lmtk::resource_sink &resource_sink)
{
    int new_pos = std::max(
      std::min(
        add_state.selected_entry_index + movement,
        (int)add_state.entries.size() - 1),
      0);

    if (add_state.selected_entry_index == new_pos)
        return false;

    auto &entry = add_state.selection();
    entry.line.set_text(
      renderer,
      font_atlas,
      fmt::format("  {}", lmng::get_type_name(entry.type)),
      resource_sink);

    add_state.selected_entry_index = new_pos;
    auto &new_entry = add_state.selection();
    new_entry.line.set_text(
      renderer,
      font_atlas,
      fmt::format(" *{}", lmng::get_type_name(new_entry.type)),
      resource_sink);
    return true;
}

void inspector::destroy(
  inspector::add_state &state,
  lmtk::resource_sink &resource_sink)
{
    state.background.move_resources(&renderer, resource_sink);
    state.title_line.move_resources(&renderer, resource_sink);
    for (auto &entry : state.entries)
        entry.line.move_resources(&renderer, resource_sink);
}

lmtk::iwidget &
  inspector::move_resources(lmgl::irenderer *, lmtk::resource_sink &sink)
{
    sink.add(&renderer, background_material);
    return *this;
}

lm::size2i inspector::get_size() { return size; }

lmtk::iwidget &inspector::set_rect(lm::point2i position, lm::size2i size)
{
    this->position = position;
    this->size = size;
    return *this;
}

lm::point2i inspector::get_position() { return position; }

std::vector<command_description> inspector::get_command_descriptions()
{
    return std::vector<command_description>();
}
} // namespace lmeditor

#pragma once

#include <entt/entity/registry.hpp>
#include <entt/meta/meta.hpp>
#include <lmeditor/inspector.h>
#include <lmengine/reflection.h>
#include <lmtk/lmtk.h>
#include <lmtk/shapes.h>
#include <lmtk/text_editor.h>
#include <lmtk/text_layout.h>

namespace lmeditor
{
class inspector : public iinspector
{
  public:
    inspector(
      lmgl::irenderer &renderer,
      lmgl::material text_material,
      lmtk::font_atlas &font_atlas,
      lm::size2i const &size);

    bool handle(
      lmtk::input_event const &event,
      lmtk::resource_sink &resource_sink,
      inspector_event_handler const &event_handler) override;

    void update(entt::registry &registry, lmtk::resource_sink &resource_sink)
      override;

    void display(
      entt::registry &registry,
      entt::entity entity,
      lmtk::resource_sink &resource_sink) override;

    void clear(lmtk::resource_sink &resource_sink) override;

    inspector &add_to_frame(lmgl::iframe *frame) override;
    lm::size2i get_size() override;
    iwidget &set_rect(lm::point2i position, lm::size2i size) override;

    lmtk::iwidget &
      move_resources(lmgl::irenderer *, lmtk::resource_sink &sink) override;

    std::vector<command_description> get_command_descriptions() override;

    static std::string
      format_component_data(std::string const &name, std::string const &repr);

    struct data_entry
    {
        std::string data_str;
        entt::meta_data data;
        unsigned line_index;
    };

    entt::meta_data &selected_data() { return selection().data; }

    data_entry &selection() { return data_entries[selected_data_index]; }

    lmtk::text_layout &selected_line()
    {
        return text_lines[selection().line_index];
    }
    void update_data_line(
      data_entry &entry,
      bool selected,
      lmtk::resource_sink &resource_sink,
      lmtk::text_layout &line_layout);

    bool move_selection(int movement, lmtk::resource_sink &resource_sink);
    void create_text(entt::registry &registry);
    void clear_text(lmtk::resource_sink &resource_sink);

    struct empty_state
    {
    };
    struct select_state
    {
    };
    struct edit_state
    {
        lmtk::text_editor editor;
    };
    // Choosing a component type to add.
    struct add_state
    {
        lmtk::rect background;
        lmtk::text_layout title_line;
        struct entry
        {
            entt::meta_type type;
            lmtk::text_layout line;
        };
        std::vector<entry> entries;
        int selected_entry_index{0};

        entry &selection() { return entries[selected_entry_index]; }
    };
    add_state create_add_state();
    void render(add_state &add_state, lmgl::iframe *frame);
    bool move_selection(
      add_state &add_state,
      int movement,
      lmtk::resource_sink &resource_sink);

    using state_variant_type =
      std::variant<empty_state, select_state, edit_state, add_state>;
    state_variant_type state;

    struct state_handle_args
    {
        lmtk::input_event const &input_event;
        lmtk::resource_sink &resource_sink;
        inspector_event_handler const &event_handler;
    };

    bool handle(inspector::empty_state &, state_handle_args const &args);

    bool handle(select_state &, state_handle_args const &args);

    bool handle(edit_state &edit_state, state_handle_args const &args);

    bool handle(inspector::add_state &add_state, state_handle_args const &args);

    lmgl::irenderer &renderer;
    lm::point2i position;
    lm::point2i get_position() override;
    lm::size2i size;
    lmgl::material text_material, background_material;
    lmtk::font_atlas &font_atlas;
    lmtk::rect background;

    entt::entity entity;
    std::vector<data_entry> data_entries;
    std::vector<lmtk::text_layout> text_lines;
    int selected_data_index{0};

    void
      destroy(inspector::add_state &state, lmtk::resource_sink &resource_sink);
    void render_base(lmgl::iframe *frame);
};
} // namespace lmeditor

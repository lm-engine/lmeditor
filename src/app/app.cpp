#include <random>

#include <lmeditor/map_editor.h>
#include <lmlib/variant_visitor.h>
#include <lmtk/text_line_selector.h>

#include "app.h"

using namespace tbb::flow;

namespace lmeditor
{
editor_app::editor_app(const std::filesystem::path &project_dir)
    : resources{project_dir},
      flow_graph(
        resources,
        [&](auto &ev) { return on_input_event(ev); },
        [&](auto frame) { return on_new_frame(frame); },
        [&]() { on_quit(); }),
      state{gui_state{*this}},
      visible_panels{{resources.map_editor.get(), resources.inspector.get()}},
      input_handlers{
        {resources.map_editor.get(),
         [](auto &app, auto &ev) { return app.map_editor_handle(ev); }},
        {resources.inspector.get(),
         [](auto &app, auto &ev) { return app.inspector_handle(ev); }},
      },
      active_panel{resources.map_editor.get()}
{
    highlight_map_editor();
}

bool editor_app::on_new_frame(lmgl::iframe *frame)
{
    state >> lm::variant_visitor{
               [&](auto &state) { state.add_to_frame(*this, frame); },
             };

    return state >> lm::variant_visitor{
                      [&](player_state &player_state) { return true; },
                      [&](auto &) { return false; },
                    };
}

bool editor_app::on_input_event(lmtk::input_event const &input_event)
{
    return state >> lm::variant_visitor{
                      [&](auto &state_alternative) {
                          return state_alternative.handle_input_event(
                            {input_event, resources.resource_sink, *this});
                      },
                    };
}

void editor_app::on_quit()
{
    state >> lm::variant_visitor{
               [&](auto &state_alternative) {
                   state_alternative.move_resources(*this);
               },
             };
    resources.free();
}

void editor_app::highlight_map_editor()
{
    resources.active_panel_border->set_rect(
      resources.map_editor->get_position(), resources.map_editor->get_size());
}

void editor_app::highlight_inspector()
{
    resources.active_panel_border->set_rect({0, 0}, resources.inspector_size);
}

void editor_app::focus_map_editor()
{
    active_panel = resources.map_editor.get();
    highlight_map_editor();
}

void editor_app::hide_inspector()
{
    visible_panels.erase(resources.inspector.get());
    resources.map_editor->set_rect({0, 0}, resources.window_size);
}

void editor_app::focus_inspector()
{
    auto const &[unused_it, did_emplace] =
      visible_panels.emplace(resources.inspector.get());

    if (did_emplace)
    {
        resources.map_editor->set_rect(
          {resources.inspector_size.width, 0}, resources.map_editor_size);
    }
    active_panel = resources.inspector.get();
    highlight_inspector();
}

void editor_app::toggle_inspector()
{
    if (active_panel == resources.inspector.get())
    {
        hide_inspector();
        focus_map_editor();
    }
    else
    {
        focus_inspector();
    }
}

void editor_app::toggle_map_editor()
{
    if (active_panel == resources.map_editor.get())
    {
        focus_inspector();
    }
    else
    {
        focus_map_editor();
    }
}

map_selector editor_app::create_map_selector()
{
    return map_selector(map_selector_init{
      .directory = resources.project_dir,
      .renderer = resources.renderer.get(),
      .font_material = resources.text_material,
      .font_atlas = resources.font_atlas,
      .rect_material = resources.rect_material,
    });
}

modal_state editor_app::create_simulation_select_state()
{
    return modal_state{
      std::make_unique<lmtk::text_line_selector>(lmtk::text_line_selector_init{
        .lines = resources.simulation_names,
        .renderer = resources.renderer.get(),
        .font_material = resources.text_material,
        .font_atlas = resources.font_atlas,
        .rect_material = resources.rect_material}),
      [](auto &app, auto widget_ptr, auto &input_event) {
          auto selector = dynamic_cast<lmtk::text_line_selector *>(widget_ptr);
          return input_event >>
                 lm::variant_visitor{
                   [&](lmtk::key_down_event const &key_down_event) {
                       if (key_down_event.key == lmpl::key_code::Enter)
                       {
                           app.resources.selected_simulation_index =
                             selector->get_selection_index();
                           selector->move_resources(
                             app.resources.renderer.get(),
                             app.resources.resource_sink);
                           app.state = gui_state{app};
                           return true;
                       }
                       return selector->handle(key_down_event);
                   },
                   [&](auto &event_alternative) {
                       return selector->handle(event_alternative);
                   }};
      }};
}

editor_app::~editor_app() {}
} // namespace lmeditor

#pragma once

#include "app.h"

namespace lmeditor
{
bool editor_app::inspector_handle(lmtk::input_event const &input_event)
{
    inspector_event_handler event_handler{
      [&](inspector_updated_data const &updated_data_event) {
          resources.map_editor->update_selection(
            updated_data_event.data, updated_data_event.string_repr);
      },
      [&](inspector_added_component const &added_component_event) {
          resources.map_editor->add_component_to_selected(
            added_component_event.type);
          resources.inspector->update(
            resources.map_editor->get_map(), resources.resource_sink);
      },
    };
    return resources.inspector->handle(
      input_event, resources.resource_sink, event_handler);
}
} // namespace lmeditor

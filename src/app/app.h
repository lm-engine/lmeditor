#pragma once

#include <filesystem>
#include <set>
#include <tbb/flow_graph.h>
#include <tbb/task_scheduler_init.h>

#include <lmeditor/map_editor.h>
#include <lmlib/application.h>

#include "resources.h"
#include <lmeditor/inspector.h>
#include <lmeditor/map_selector.h>
#include <lmeditor/simulation.h>
#include <lmeditor/trimesh.h>
#include <lmhuv.h>
#include <lmlib/realtime_clock.h>
#include <lmtk/app_flow_graph.h>
#include <lmtk/rect_border.h>
#include <lmtk/text_layout.h>

namespace lmeditor
{
class editor_app;

struct state_handle_args
{
    lmtk::input_event const &input_event;
    lmtk::resource_sink &resource_sink;
    editor_app &app;
};

struct gui_state
{
    explicit gui_state(editor_app &app);
    void add_to_frame(editor_app &app, lmgl::iframe *frame);
    bool handle_input_event(state_handle_args const &args);
    void move_resources(editor_app &app);
};
struct modal_state
{
    bool handle_input_event(state_handle_args const &args);
    std::unique_ptr<lmtk::iwidget> modal;
    std::function<
      bool(editor_app &, lmtk::iwidget *, lmtk::input_event const &)>
      input_handler;
    void add_to_frame(editor_app &app, lmgl::iframe *frame);
    void move_resources(editor_app &app);
};
struct player_state
{
    entt::registry registry;
    psimulation simulation;
    lmhuv::pvisual_view visual_view;

    lm::realtime_clock clock;

    bool handle_input_event(state_handle_args const &args);

    void update_simulation(lmtk::input_state const &input_state);

    void move_resources(editor_app &app);
    void add_to_frame(editor_app &app, lmgl::iframe *frame);
};

class editor_app
{
    friend class gui_state;
    friend class modal_state;
    friend class player_state;

    using state_variant_type =
      std::variant<gui_state, modal_state, player_state>;

    editor_app_resources resources;
    lmtk::app_flow_graph flow_graph;

    state_variant_type state;
    std::set<lmtk::iwidget *> visible_panels;
    std::map<
      lmtk::iwidget *,
      std::function<bool(editor_app &, lmtk::input_event const &)>>
      input_handlers;
    itool_panel *active_panel;

    player_state create_player_state();
    modal_state create_simulation_select_state();

    bool map_editor_handle(lmtk::input_event const &input_event);
    bool inspector_handle(lmtk::input_event const &input_event);

    void highlight_map_editor();
    void highlight_inspector();

    void toggle_inspector();
    void toggle_map_editor();

    map_selector create_map_selector();

  protected:
    bool on_input_event(lmtk::input_event const &variant);
    bool on_new_frame(lmgl::iframe *frame);
    void on_quit();

  public:
    editor_app(const std::filesystem::path &project_dir);
    editor_app(editor_app const &) = delete;
    void main() { flow_graph.enter(); }
    ~editor_app();
    void hide_inspector();
    void focus_map_editor();
    void focus_inspector();
};
} // namespace lmeditor

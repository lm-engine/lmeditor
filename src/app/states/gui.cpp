#include "../app.h"
#include <lmeditor/map_selector.h>

namespace lmeditor
{
bool gui_state::handle_input_event(state_handle_args const &args)
{
    if (
      args.input_event >>
      lm::variant_visitor{
        [&](lmtk::key_down_event const &key_down_event) {
            auto map_file_path = args.app.resources.project_dir / "map.yml";
            switch (key_down_event.key)
            {
            case lmpl::key_code::I:
                if (key_down_event.input_state.key_state.alt())
                {
                    args.app.toggle_inspector();
                    return true;
                }
                return false;

            case lmpl::key_code::M:
                if (key_down_event.input_state.key_state.alt())
                {
                    args.app.toggle_map_editor();
                    return true;
                }
                return false;

            case lmpl::key_code::F1:
                args.app.state =
                  modal_state{std::make_unique<command_help>(command_help_init{
                    .renderer = *args.app.resources.renderer,
                    .material = args.app.resources.text_material,
                    .font_atlas = args.app.resources.font_atlas,
                    .commands =
                      args.app.active_panel->get_command_descriptions(),
                  })};
                return true;

            case lmpl::key_code::P:
                if (key_down_event.input_state.key_state.control())
                {
                    args.app.state = args.app.create_simulation_select_state();
                    return true;
                }
                args.app.state = args.app.create_player_state();
                return true;

            case lmpl::key_code::L:
                if (key_down_event.input_state.key_state.control())
                {
                    args.app.state.emplace<modal_state>(modal_state{
                      std::make_unique<map_selector>(
                        args.app.create_map_selector()),
                      [](editor_app &app, auto widget, auto &input_event) {
                          return dynamic_cast<map_selector *>(widget)->handle(
                            input_event,
                            map_selector_event_handler{
                              [&](map_selector_chose_map const &ev) {
                                  app.resources.map_editor->load_map(
                                    ev.path_to_file,
                                    app.resources.renderer.get(),
                                    app.resources.resource_sink);
                                  app.state.emplace<gui_state>(app);
                              }});
                      }});
                    return true;
                }
                return false;

            case lmpl::key_code::S:
                if (key_down_event.input_state.key_state.control())
                {
                    args.app.resources.map_editor->save_map(map_file_path);
                    return true;
                }
                return false;

            default:
                return false;
            }
        },
        [](auto) { return false; },
      })
        return true;

    return args.app.input_handlers[args.app.active_panel](
      args.app, args.input_event);
}

gui_state::gui_state(editor_app &app) { app.highlight_map_editor(); }

void gui_state::add_to_frame(editor_app &app, lmgl::iframe *frame)
{
    for (auto &ppanel : app.visible_panels)
        ppanel->add_to_frame(frame);

    app.resources.active_panel_border->add_to_frame(frame);
}

void gui_state::move_resources(editor_app &app) {}
} // namespace lmeditor

#include "orbital_camera.h"
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <iostream>

namespace lmeditor
{
orbital_camera::orbital_camera(const orbital_camera_init &init)
    : camera{lm::camera_init{
        .fov = init.fov,
        .aspect = init.aspect,
        .near_clip = init.near_clip,
        .far_clip = init.far_clip,
        .position = get_pos(init.euler_angles, init.target, init.distance),
        .euler_angles = init.euler_angles,
      }},
      distance{init.distance}
{
}

orbital_camera &orbital_camera::rotate(const Eigen::Vector3f &delta_angles)
{
    Eigen::Vector3f target = pos + lm::rotation_matrix(euler_angles) *
                                     Eigen::Vector3f{0.f, 0.f, distance};

    euler_angles += delta_angles;
    pos = get_pos(euler_angles, target, distance);
    return *this;
}

Eigen::Vector3f orbital_camera::get_pos(
  const Eigen::Vector3f &angles,
  const Eigen::Vector3f &target,
  float distance)
{
    return lm::rotation_matrix(angles) * Eigen::Vector3f{0.f, 0.f, -distance} +
           target;
}

orbital_camera &orbital_camera::move(const float distance)
{
    this->distance += distance;
    pos +=
      lm::rotation_matrix(euler_angles) * Eigen::Vector3f{0.f, 0.f, -distance};
    return *this;
}

orbital_camera &orbital_camera::move_to_target(const Eigen::Vector3f &target)
{
    pos = get_pos(euler_angles, target, distance);
    return *this;
}
} // namespace lmeditor
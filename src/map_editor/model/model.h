#pragma once

#include "../../viewport/viewport.h"
#include <entt/entity/entity.hpp>
#include <entt/entity/fwd.hpp>
#include <lmeditor/map_editor.h>
#include <lmeditor/map_editor_events.h>
#include <lmengine/shapes.h>
#include <lmengine/transform.h>
#include <variant>

namespace lmeditor
{
class map_editor_model : public viewport
{
  public:
    struct init;

    explicit map_editor_model(init const &init);

    bool handle(
      lmtk::input_event const &event,
      map_editor_event_handler const &event_handler = {});

    void add_component_to_selected(entt::meta_type const &type);

    entt::entity add_cube(Eigen::Vector3f const &position, float extent);

    bool update_selection(
      entt::meta_data const &data,
      std::string const &string_repr);

    entt::registry map;
    entt::entity selected_box{entt::null};

    void select_box(entt::entity entity) { selected_box = entity; }

    bool have_selection() { return map.valid(selected_box); }

    void translate(entt::entity entity, Eigen::Vector3f const &vector);

    Eigen::Vector3f view_to_world(Eigen::Vector3f const &view_vector)
    {
        return lm::rotation_matrix(camera.euler_angles) * view_vector;
    }

    Eigen::Vector3f view_to_axis(Eigen::Vector3f const &view_vector);

    entt::entity
      nearest_entity(entt::entity entity, const Eigen::Vector3f &direction);
    entt::entity farthest_entity(Eigen::Vector3f const &direction);
    bool move_selection(
      Eigen::Vector3f const &direction,
      map_editor_event_handler const &event_handler = {});
    bool move_selection_view(
      const Eigen::Vector3f &view_axis,
      map_editor_event_handler const &event_handler);

    void load_map(std::filesystem::path const &file_path);
    void save_map(std::filesystem::path const &file_path);

    entt::entity add_adjacent(Eigen::Vector3f const &direction);
    entt::entity copy_object(Eigen::Vector3f const &direction);

    std::vector<command_description> get_command_descriptions();

    Eigen::Vector3f get_selection_position()
    {
        return map.get<lmng::transform>(selected_box).position;
    }

    Eigen::Vector3f get_selection_extents()
    {
        return map.get<lmng::box_render>(selected_box).extents;
    }

    struct command_args
    {
        void *state_ptr;
        class map_editor_model &model;
        map_editor_event_handler const &event_handler;
        lmtk::key_down_event const &key_down_event;
    };

    struct command
    {
        std::function<bool(command_args const &)> fn;
        std::string description;
    };

    using command_map = std::unordered_map<lmpl::key_code, command>;
    using command_list = std::vector<std::pair<lmpl::key_code, command>>;

    static command_list viewport_commands;

    static std::vector<command_description> get_command_descriptions(
      command_list const &list,
      std::string const &context);

    struct select_state
    {
        constexpr static auto label = "-";

        template <typename other_state_type>
        explicit select_state(other_state_type &, map_editor_model &model)
            : select_state{model} {};

        explicit select_state(map_editor_model &model);

        auto get_command_descriptions()
        {
            return map_editor_model::get_command_descriptions(
              commands, "Map Editor");
        }

        command_list commands;
        command_map key_command_map;
    };
    struct move_selected_state
    {
        using parent_state_type = select_state;
        constexpr static auto label = "Move Object";

        explicit move_selected_state(select_state &, map_editor_model &);

        auto get_command_descriptions()
        {
            return map_editor_model::get_command_descriptions(
              commands, "Map Editor -> Moving Object");
        }

        command_list commands;
        command_map key_command_map;
    };
    struct add_adjacent_state
    {
        constexpr static auto label = "Choose Direction";
        using parent_state_type = select_state;

        explicit add_adjacent_state(
          map_editor_model::select_state &,
          map_editor_model &model);

        auto get_command_descriptions()
        {
            return map_editor_model::get_command_descriptions(
              commands, "Map Editor -> Add Object");
        }

        command_list commands;
        command_map key_command_map;
    };
    struct copy_object_state
    {
        constexpr static auto label = "Choose Direction";
        using parent_state_type = select_state;

        copy_object_state(
          map_editor_model::select_state &,
          map_editor_model &model);

        auto get_command_descriptions()
        {
            return map_editor_model::get_command_descriptions(
              commands, "Map Editor -> Copy Object");
        }

        command_list commands;
        command_map key_command_map;
    };

    using state_variant_type = std::variant<
      select_state,
      move_selected_state,
      add_adjacent_state,
      copy_object_state>;

    state_variant_type state;

    template <typename new_state_type, typename current_state_type>
    auto enter_state(current_state_type &current_state);

    template <typename state_type> void leave_state(state_type &current_state);
};

template <typename new_state_type, typename current_state_type>
auto map_editor_model::enter_state(current_state_type &current_state)
{
    static_assert(
      std::is_same_v<
        current_state_type,
        typename new_state_type::parent_state_type>,
      "New state must have current state as parent.");

    state = new_state_type{current_state, *this};
    return true;
}

template <typename state_type>
void map_editor_model::leave_state(state_type &current_state)
{
    using new_state_type = typename state_type::parent_state_type;
    state = new_state_type{current_state, *this};
}

struct map_editor_model::init
{
    orbital_camera_init camera_init;
};
} // namespace lmeditor

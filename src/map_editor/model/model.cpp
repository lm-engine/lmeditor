#include "model.h"
#include <lmengine/physics.h>
#include <lmengine/reflection.h>
#include <lmlib/eigen.h>

namespace lmeditor
{
map_editor_model::map_editor_model(init const &init)
    : state{select_state{*this}},
      viewport{{init.camera_init}},
      selected_box{entt::null}
{
}

bool map_editor_model::handle(
  lmtk::input_event const &event,
  map_editor_event_handler const &event_handler)
{
    return std::visit(
      lm::variant_visitor{
        [&](
          lmtk::key_down_event const &key_down_event, auto &state_alternative) {
            auto itfound =
              state_alternative.key_command_map.find(key_down_event.key);

            if (itfound == state_alternative.key_command_map.end())
                return false;

            return itfound->second.fn(command_args{
              &state_alternative, *this, event_handler, key_down_event});
        },
        [](auto &, auto &) { return false; },
      },
      event,
      state);
}

void map_editor_model::add_component_to_selected(entt::meta_type const &type)
{
    auto component = type.ctor().invoke();
    lmng::assign_to_entity(component, map, selected_box);
    map.get<lmng::meta_component>(selected_box)
      .component_types.emplace_back(type);
}

bool map_editor_model::update_selection(
  entt::meta_data const &data,
  std::string const &string_repr)
{
    auto component = lmng::any_component{map, selected_box, data.parent()};
    component.set(data, string_repr);
    component.replace(map, selected_box);
    return true;
}

entt::entity
  map_editor_model::add_cube(Eigen::Vector3f const &position, float extent)
{
    auto new_cube = map.create();

    auto &transform = map.assign<lmng::transform>(
      new_cube, position, Eigen::Quaternionf::Identity());

    auto &box = map.assign<lmng::box_render>(
      new_cube,
      Eigen::Vector3f{extent, extent, extent},
      std::array{0.5f, 0.5f, 0.5f});

    auto &rigid_body =
      map.assign<lmng::rigid_body>(new_cube, 1.f, 0.25f, 0.75f);

    map.assign<lmng::meta_component>(
      new_cube,
      std::vector{{
        entt::resolve<lmng::transform>(),
        entt::resolve<lmng::box_render>(),
        entt::resolve<lmng::rigid_body>(),
      }});

    return new_cube;
}

entt::entity map_editor_model::add_adjacent(Eigen::Vector3f const &direction)
{
    auto add_pos = get_selection_position();
    auto extents = get_selection_extents();
    extents += Eigen::Vector3f::Ones();
    add_pos += std::abs(extents.dot(direction)) * direction;
    return add_cube(add_pos, 1.f);
}

entt::entity map_editor_model::copy_object(Eigen::Vector3f const &direction)
{
    auto new_box = map.create(selected_box, map);
    auto &transform = map.get<lmng::transform>(new_box);
    transform.position +=
      2 * Eigen::Vector3f{direction.array() * get_selection_extents().array()};
    return new_box;
}

void map_editor_model::translate(
  entt::entity entity,
  Eigen::Vector3f const &vector)
{
    map.get<lmng::transform>(entity).position += vector;
}

std::vector<command_description> map_editor_model::get_command_descriptions()
{
    return state >> lm::variant_visitor{[](auto &state) {
               return state.get_command_descriptions();
           }};
}

std::vector<command_description> map_editor_model::get_command_descriptions(
  map_editor_model::command_list const &list,
  std::string const &context)
{
    std::vector<command_description> command_lines;
    for (auto &[key_code, command] : list)
    {
        command_lines.emplace_back(command_description{
          command.description,
          std::string{lmpl::key_code_name_map[key_code]},
          context,
        });
    }
    return std::move(command_lines);
}

Eigen::Vector3f
  map_editor_model::view_to_axis(Eigen::Vector3f const &view_vector)
{
    return lm::snap_to_axis(view_to_world(view_vector));
}
} // namespace lmeditor

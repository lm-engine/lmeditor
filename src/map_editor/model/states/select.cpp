#include "../model.h"

#include <range/v3/view/concat.hpp>

namespace lmeditor
{

map_editor_model::command move_selection_back_command{
  [](map_editor_model::command_args const &args) {
      return args.model.move_selection_view(
        -Eigen::Vector3f::UnitZ(), args.event_handler);
  },
  "Move selection back",
};

map_editor_model::command move_selection_forward_command{
  [](map_editor_model::command_args const &args) {
      return args.model.move_selection_view(
        Eigen::Vector3f::UnitZ(), args.event_handler);
  },
  "Move selection forward",
};

map_editor_model::command move_selection_up_command{
  [](map_editor_model::command_args const &args) {
      return args.model.move_selection_view(
        Eigen::Vector3f::UnitY(), args.event_handler);
  },
  "Move selection up",
};

map_editor_model::command move_selection_down_command{
  [](map_editor_model::command_args const &args) {
      return args.model.move_selection_view(
        -Eigen::Vector3f::UnitY(), args.event_handler);
  },
  "Move selection down",
};

map_editor_model::command move_selection_left_command{
  [](map_editor_model::command_args const &args) {
      return args.model.move_selection_view(
        -Eigen::Vector3f::UnitX(), args.event_handler);
  },
  "Move selection left",
};

map_editor_model::command move_selection_right_command{
  [](map_editor_model::command_args const &args) {
      return args.model.move_selection_view(
        Eigen::Vector3f::UnitX(), args.event_handler);
  },
  "Move selection right",
};

map_editor_model::command delete_selected_command{
  [](map_editor_model::command_args const &args) {
      if (args.model.have_selection())
      {
          args.event_handler(map_editor_destroying_entity{
            args.model.map, args.model.selected_box});
          args.model.map.destroy(args.model.selected_box);
          args.model.selected_box = entt::null;
          return true;
      }
      return false;
  },
  "Delete selected",
};

map_editor_model::command add_command{
  [](map_editor_model::command_args const &args) {
      Eigen::Vector3f add_pos = Eigen::Vector3f::Zero();
      if (args.model.have_selection())
      {
          args.model.template enter_state<map_editor_model::add_adjacent_state>(
            *static_cast<map_editor_model::select_state *>(args.state_ptr));
          return true;
      }

      entt::entity new_box = args.model.add_cube(add_pos, 1.f);
      args.event_handler(map_editor_created_entity{args.model.map, new_box});

      args.model.selected_box = new_box;
      args.event_handler(map_editor_changed_selection{args.model.map, new_box});

      return true;
  },
  "Add object",
};

map_editor_model::command deselect_command{
  [](map_editor_model::command_args const &args) {
      if (args.model.have_selection())
      {
          args.model.selected_box = entt::null;
          args.event_handler(map_editor_cleared_selection{});
          return true;
      }
      return false;
  },
  "Deselect",
};

map_editor_model::command move_selected_command{
  [](map_editor_model::command_args const &args) {
      if (args.model.have_selection())
      {
          args.model
            .template enter_state<map_editor_model::move_selected_state>(
              *static_cast<map_editor_model::select_state *>(args.state_ptr));
          return true;
      }
      else
          return false;
  },
  "Move selected",
};

map_editor_model::command copy_object_command{
  [](map_editor_model::command_args const &args) {
      Eigen::Vector3f add_pos = Eigen::Vector3f::Zero();
      if (args.model.have_selection())
      {
          args.model.template enter_state<map_editor_model::copy_object_state>(
            *static_cast<map_editor_model::select_state *>(args.state_ptr));
          return true;
      }

      return false;
  },
  "Copy object",
};

map_editor_model::command_list select_commands{
  {lmpl::key_code::U, move_selection_back_command},
  {lmpl::key_code::O, move_selection_forward_command},
  {lmpl::key_code::I, move_selection_up_command},
  {lmpl::key_code::K, move_selection_down_command},
  {lmpl::key_code::J, move_selection_left_command},
  {lmpl::key_code::L, move_selection_right_command},
  {lmpl::key_code::X, delete_selected_command},
  {lmpl::key_code::A, add_command},
  {lmpl::key_code::N, deselect_command},
  {lmpl::key_code::M, move_selected_command},
  {lmpl::key_code::C, copy_object_command},
};

map_editor_model::select_state::select_state(map_editor_model &map_editor)
    : commands{ranges::view::concat(
        map_editor_model::viewport_commands,
        select_commands)},
      key_command_map{ranges::view::all(commands)}
{
}
} // namespace lmeditor

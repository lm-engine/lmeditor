#include "model.h"

#include <lmengine/transform.h>
#include <range/v3/algorithm/lower_bound.hpp>
#include <range/v3/algorithm/sort.hpp>
#include <range/v3/algorithm/upper_bound.hpp>

namespace lmeditor
{
entt::entity map_editor_model::nearest_entity(
  entt::entity entity,
  const Eigen::Vector3f &direction)
{
    auto transforms = map.view<lmng::transform>();

    if (transforms.size() == 1)
    {
        return entt::null;
    }

    struct nearby_entity
    {
        entt::entity entity;
        float to_dot_dir;
        float to_mag;
    };

    std::vector<nearby_entity> entities_in_dir;

    entities_in_dir.reserve(transforms.size());

    lmng::transform transform = transforms.get(entity);

    transforms.each([&](auto other_entity, auto &other_transform) {
        if (other_entity == entity)
            return;

        Eigen::Vector3f to_other =
          other_transform.position - transform.position;

        float to_dot_dir = to_other.dot(direction);

        if (to_dot_dir > 0.01f)
        {
            entities_in_dir.emplace_back(
              nearby_entity{other_entity, to_dot_dir, to_other.norm()});
        }
    });

    if (entities_in_dir.empty())
        return entt::null;

    ranges::sort(entities_in_dir, [](nearby_entity &lhs, nearby_entity &rhs) {
        return lhs.to_mag < rhs.to_mag;
    });
    ranges::sort(entities_in_dir, [](nearby_entity &lhs, nearby_entity &rhs) {
        return lhs.to_dot_dir < rhs.to_dot_dir;
    });

    return entities_in_dir.front().entity;
}

entt::entity map_editor_model::farthest_entity(const Eigen::Vector3f &direction)
{
    auto transforms = map.view<lmng::transform>();

    if (transforms.empty())
        return entt::null;
    else if (transforms.size() == 1)
        return *transforms.begin();

    std::vector<std::pair<entt::entity, float>> entities_in_dir;

    entities_in_dir.reserve(transforms.size());

    transforms.each([&](auto curr, auto &transform) {
        float curr_along = transform.position.dot(direction);
        auto entry = std::pair{curr, curr_along};
        entities_in_dir.insert(
          ranges::upper_bound(
            entities_in_dir,
            entry,
            [](auto const &lhs, auto const &rhs) {
                return lhs.second > rhs.second;
            }),
          entry);
    });

    return entities_in_dir.front().first;
}

bool map_editor_model::move_selection(
  Eigen::Vector3f const &direction,
  map_editor_event_handler const &event_handler)
{
    if (!have_selection())
    {
        auto farthest = farthest_entity(direction);
        if (!map.valid(farthest))
            return false;

        select_box(farthest);
        event_handler(map_editor_changed_selection{map, selected_box});
        return true;
    }

    auto nearest = nearest_entity(
      selected_box, lm::rotation_matrix(camera.euler_angles) * direction);

    if (!map.valid(nearest))
        return false;

    select_box(nearest);
    event_handler(map_editor_changed_selection{map, selected_box});
    return true;
}

bool map_editor_model::move_selection_view(
  const Eigen::Vector3f &view_axis,
  map_editor_event_handler const &event_handler)
{
    return move_selection(
      lm::rotation_matrix(camera.euler_angles) * view_axis, event_handler);
}
} // namespace lmeditor

#include "../model.h"

#include <catch2/catch.hpp>
#include <range/v3/all.hpp>

class model_test_case
{
  protected:
    lmeditor::map_editor_model model;

  public:
    model_test_case() : model{{}} {}
};

TEST_CASE_METHOD(model_test_case, "Add cube position")
{
    Eigen::Vector3f cube_pos{2.f, 0.f, 0.f};
    auto cube_id = model.add_cube(cube_pos, 1.f);

    REQUIRE(model.map.get<lmng::transform>(cube_id).position == cube_pos);
}

TEST_CASE_METHOD(model_test_case, "Map editor select mode")
{
    lmtk::input_state input_state{};

    model.handle(lmtk::key_down_event{
      .input_state = input_state,
      .key = lmpl::key_code::V,
    });
    REQUIRE(std::holds_alternative<lmeditor::map_editor_model::select_state>(
      model.state));
}

TEST_CASE_METHOD(model_test_case, "Map editor select box")
{
    lmtk::input_state input_state{};

    auto cube_id = model.add_cube({0.f, 0.f, 0.f}, 1.f);

    model.handle(lmtk::key_down_event{
      .input_state = input_state,
      .key = lmpl::key_code::V,
    });

    model.handle(lmtk::key_down_event{
      .input_state = input_state,
      .key = GENERATE(
        lmpl::key_code::J,
        lmpl::key_code::L,
        lmpl::key_code::I,
        lmpl::key_code::K,
        lmpl::key_code::U,
        lmpl::key_code::O),
    });

    REQUIRE(model.selected_box == cube_id);
}

TEST_CASE_METHOD(model_test_case, "Map editor select outer")
{
    auto first_cube = model.add_cube({0.f, 0.f, 0.f}, 1.f);
    auto second_cube = model.add_cube({2.f, 2.f, 2.f}, 1.f);

    lmtk::input_state input_state{};

    model.handle(lmtk::key_down_event{
      .input_state = input_state,
      .key = lmpl::key_code::V,
    });

    std::array cubes{first_cube, second_cube};

    auto [key, cube_index] = GENERATE(table<lmpl::key_code, int>({
      {lmpl::key_code::J, 0},
      {lmpl::key_code::L, 1},
      {lmpl::key_code::I, 1},
      {lmpl::key_code::K, 0},
      {lmpl::key_code::U, 0},
      {lmpl::key_code::O, 1},
    }));

    model.handle(lmtk::key_down_event{
      .input_state = input_state,
      .key = key,
    });

    CAPTURE(key, cube_index);

    REQUIRE(model.selected_box == cubes[cube_index]);
}

TEST_CASE_METHOD(model_test_case, "Map Editor Move Selection")
{
    auto axis = GENERATE(
      Eigen::Vector3f::UnitX(),
      Eigen::Vector3f::UnitY(),
      Eigen::Vector3f::UnitZ());

    auto move_direction = GENERATE(1, -1);
    auto initial_selection = GENERATE(0, 1, 2);

    std::array cubes = {
      model.add_cube(-2 * axis, 1.f),
      model.add_cube(Eigen::Vector3f::Zero(), 1.f),
      model.add_cube(2 * axis, 1.f),
    };

    model.select_box(cubes[initial_selection]);

    auto expected_selection =
      std::max(std::min(initial_selection + move_direction, 2), 0);

    std::vector<Eigen::Matrix<float, 1, 3>> positions;
    for (auto cube : cubes)
        positions.emplace_back(
          model.map.get<lmng::transform>(cube).position.transpose());

    model.move_selection(move_direction * axis);

    auto actual_selection =
      ranges::find(cubes, model.selected_box) - cubes.begin();

    CAPTURE(
      positions,
      axis.transpose(),
      initial_selection,
      move_direction,
      actual_selection,
      expected_selection);

    REQUIRE(actual_selection == expected_selection);
}

TEST_CASE_METHOD(model_test_case, "Minimum select distance")
{
    std::array cubes{
      model.add_cube({-2.f, 0.f, 0.f}, 1.f),
      model.add_cube({2.f, 0.01f, 0.f}, 1.f),
      model.add_cube({0.f, -2.f, 0.f}, 1.f),
    };

    model.select_box(cubes[0]);

    model.move_selection({0.f, -1.f, 0.f});

    REQUIRE(model.selected_box == cubes[2]);
}

TEST_CASE_METHOD(model_test_case, "Map editor deselect")
{
    auto cube = model.add_cube({0.f, 0.f, 0.f}, 1.f);

    lmtk::input_state input_state{};

    model.handle(lmtk::key_down_event{
      .input_state = input_state,
      .key = lmpl::key_code::V,
    });

    model.handle(lmtk::key_down_event{
      .input_state = input_state,
      .key = lmpl::key_code::L,
    });

    model.handle(lmtk::key_down_event{
      .input_state = input_state,
      .key = lmpl::key_code::N,
    });

    REQUIRE(model.selected_box != cube);
}

TEST_CASE_METHOD(model_test_case, "Add new fires entity added event")
{
    lmtk::input_state const &input_state{};
    bool entity_created_fired{false};

    model.handle(
      lmtk::key_down_event{
        .input_state = input_state,
        .key = lmpl::key_code::A,
      },
      lmeditor::map_editor_event_handler{}.set_on_created_entity(
        [&](lmeditor::map_editor_created_entity const &) {
            entity_created_fired = true;
        }));

    REQUIRE(entity_created_fired);
}

TEST_CASE_METHOD(
  model_test_case,
  "Destroy current fires destroying entity event")
{
    lmtk::input_state const &input_state{};
    bool destroying_entity_fired{false};
    auto box = model.add_cube({0.f, 0.f, 0.f}, 1.f);

    model.select_box(box);
    model.handle(
      lmtk::key_down_event{
        .input_state = input_state,
        .key = lmpl::key_code::X,
      },
      lmeditor::map_editor_event_handler{}.set_on_destroying_entity(
        [&](lmeditor::map_editor_destroying_entity const &) {
            destroying_entity_fired = true;
        }));

    REQUIRE(destroying_entity_fired);
}

#include "model.h"

namespace lmeditor
{
map_editor_model::command orbit_right_command{
  [](map_editor_model::command_args const &args) {
      args.model.camera.rotate(Eigen::Vector3f{0.f, -0.05f, 0.f});
      return true;
  },
  "Orbit camera right",
};
map_editor_model::command orbit_left_command{
  [](map_editor_model::command_args const &args) {
      args.model.camera.rotate(Eigen::Vector3f{0.f, 0.05f, 0.f});
      return true;
  },
  "Orbit camera left",
};
map_editor_model::command orbit_down_command{
  [](map_editor_model::command_args const &args) {
      args.model.camera.rotate(Eigen::Vector3f{-0.05f, 0.f, 0.f});
      return true;
  },
  "Orbit camera down",
};
map_editor_model::command orbit_up_command{
  [](map_editor_model::command_args const &args) {
      args.model.camera.rotate(Eigen::Vector3f{0.05f, 0.f, 0.f});
      return true;
  },
  "Orbit camera up",
};
map_editor_model::command move_camera_forward_command{
  [](map_editor_model::command_args const &args) {
      args.model.move_camera_closer(0.1f);
      return true;
  },
  "Move camera forward",
};
map_editor_model::command move_camera_back_command{
  [](map_editor_model::command_args const &args) {
      args.model.move_camera_further(0.1f);
      return true;
  },
  "Move camera back",
};
map_editor_model::command move_to_selection_command{
  [](map_editor_model::command_args const &args) {
      if (args.model.have_selection())
          args.model.camera.move_to_target(
            args.model.map.get<lmng::transform>(args.model.selected_box)
              .position);
      return true;
  },
  "Move camera to selection",
};

map_editor_model::command_list map_editor_model::viewport_commands{
  {lmpl::key_code::S, orbit_left_command},
  {lmpl::key_code::F, orbit_right_command},
  {lmpl::key_code::E, orbit_up_command},
  {lmpl::key_code::D, orbit_down_command},
  {lmpl::key_code::R, move_camera_forward_command},
  {lmpl::key_code::W, move_camera_back_command},
  {lmpl::key_code::G, move_to_selection_command},
};
} // namespace lmeditor

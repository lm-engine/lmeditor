#include "model/model.h"
#include "view/view.h"

namespace lmeditor
{
class map_editor : public imap_editor
{
    map_editor_model model;
    map_editor_view view;

  public:
    map_editor(map_editor_init const &init)
        : model{{orbital_camera_init{
            .fov = (float)M_PI / 3,
            .aspect = (float)init.size.width / init.size.height,
            .near_clip = 0.1f,
            .far_clip = 1000.f,
            .distance = 30.f,
            .target = Eigen::Vector3f{0.f, 0.f, 0.f},
            .euler_angles = Eigen::Vector3f{M_PI / 8.f, 0.f, 0.f},
          }}},
          view{{init, model.map}}
    {
    }

    bool handle(
      lmtk::input_event const &event,
      lmtk::resource_sink &resource_sink,
      map_editor_event_handler const &event_handler) override
    {
        return model.handle(
          event,
          event_handler.copy()
            .set_on_created_entity([&](auto &ev) {
                view.add_box(ev.map, ev.entity);
                event_handler.on_created_entity(ev);
            })
            .set_on_destroying_entity([&](auto &ev) {
                view.visual_view->destroy_box(
                  view.renderer, ev.entity, resource_sink);
            }));
    };

    bool update_selection(
      entt::meta_data const &data,
      std::string const &string_repr) override
    {
        return model.update_selection(data, string_repr);
    };

    void add_component_to_selected(entt::meta_type const &type) override
    {
        model.add_component_to_selected(type);
    };

    entt::registry &get_map() override { return model.map; };

    map_editor &add_to_frame(lmgl::iframe *frame) override
    {
        view.render(frame, model);
        return *this;
    }

    imap_editor &set_rect(lm::point2i position, lm::size2i size) override
    {
        view.position = position;
        view.size = size;
        model.camera.aspect = (float)size.width / (float)size.height;
        return *this;
    };

    map_editor &
      move_resources(lmgl::irenderer *, lmtk::resource_sink &sink) override
    {
        view.move_resources(sink);
        return *this;
    };
    void load_map(
      std::filesystem::path const &file_path,
      lmgl::irenderer *renderer,
      lmtk::resource_sink &resource_sink) override
    {
        view.visual_view->clear(renderer, resource_sink);
        model.load_map(file_path);

        model.map.view<lmng::box_render>().each(
          [&](auto entity, auto &box) { view.add_box(model.map, entity); });
    }
    void save_map(std::filesystem::path const &file_path) override
    {
        model.save_map(file_path);
    }
    std::vector<command_description> get_command_descriptions() override
    {
        return model.get_command_descriptions();
    }
    lm::size2i get_size() override { return view.size; }
    lm::point2i get_position() override { return view.position; }
};

pmap_editor create_map_editor(map_editor_init const &init)
{
    return std::make_unique<map_editor>(init);
}
} // namespace lmeditor
